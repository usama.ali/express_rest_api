const express = require('express')
const app = express()
const port = 3000
var supplier = require('./routes/api/supplier.js')

app.use(express.json());
app.use(express.urlencoded({extended: true})); //Parse URL-encoded bodies

app.use('/suppliers',supplier)

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))