var express = require('express');
var router = express.Router();


var mysql      = require('mysql2');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password:'root',
    database: 'metro'
  });
   
 




// [POST] endpoint/users
router.post('/',async (req,res)=>{
    const suppId=req.body.suppId
    const name=req.body.name
    
    try{
        result=await connection.promise().execute('insert into dim_supplier values(?,?)',[suppId,name])
        res.json({success:true})
    }
    catch(err){
        console.log(err)
        res.status(400).json({error:"Error occurred. Try again"})
    }

})

// [GET] endpoint/users/:userId (get specific user)
router.get('/:suppid', async (req,res)=> {
    var suppId = req.params.suppid;
    console.log(suppId)
    try{
        const [rows, fields] = await connection.promise().query("select * from dim_supplier where SUPPLIER_ID = ?",[suppId])
        if(rows.length==0){
            res.status(400).json({error:"Failed to found Supplier ID:"+suppId})
        }
        else{
            res.json(rows)
        }
    
    }
    catch(err){
        console.log(err)
        res.status(400).json({error:"Error occurred. Try again"})
    }
    
})

// [PATCH] endpoint/users/:userId (update the data for the specified user)
router.patch('/:suppId',async (req,res)=>{
    const suppId=req.params.suppId
    const name=req.body.name

    try{
        result=await connection.promise().execute('update dim_supplier set supplier_name=? where supplier_id=?',[name,suppId])
        res.json({success:true})
    }
    catch(err){
        console.log(err)
        res.status(400).json({error:"Error occurred. Try again"})
    }

})
// [DELETE] endpoint/users/:userId (remove the specified user)
router.delete('/:suppId',async (req,res)=>{
    const suppId=req.params.suppId
    const name=req.body.name

    try{
        result=await connection.promise().execute('delete from dim_supplier where supplier_id=?',[suppId])
        res.json({success:true})
    }
    catch(err){
        console.log(err)
        res.status(400).json({error:"Error occurred. Try again"})
    }

})

// [GET] endpoint/users (list users)
//return list
router.get('/', async (req,res)=> {

    try{
        const [rows, fields] = await connection.promise().query('select * from dim_supplier')
        res.json(rows)
    
    }
    catch(err){
        console.log(err)
        res.status(400).json({error:"Error occurred. Try again"})
    }
    
})

module.exports = router;